module gitlab.com/avantys/testgateway/application-go
go 1.16
require (
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.5
)
